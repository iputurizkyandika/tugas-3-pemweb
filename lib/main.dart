import 'package:flutter/material.dart';
import './detailproduk.dart';
import './produklist.dart';
import 'home1.dart';

void main() {
  runApp(new MaterialApp(
      home: new Halsatu(),
      title: "cakeshop",
      routes: <String, WidgetBuilder>{
        '/Halsatu': (BuildContext context) => new Halsatu(),
        '/About': (BuildContext context) => new About(),
        '/produklist': (BuildContext context) => new Produklist(),
        '/kontak': (BuildContext context) => new Kontak(),
        '/custom': (BuildContext context) => new Custom(),
        '/pengiriman': (BuildContext context) => new Pengiriman(),
        '/home1': (BuildContext context) => new Home(),
      }));
}

class Halsatu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title:
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Text(
            "CakeShop.",
            style: TextStyle(fontSize: 30),
          ),
        ]),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search_rounded,
              size: 30,
            ),
            onPressed: () {},
          ),
        ],
        backgroundColor: Colors.red[400],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("I Putu Rizky Andika"),
              accountEmail: new Text("rizky.andika@undiksha.ac.id"),
              currentAccountPicture: new GestureDetector(
                onTap: () {},
                child: new CircleAvatar(
                  backgroundColor: Colors.red[400],
                  backgroundImage: AssetImage("assets/appimages/profile.jpg"),
                ),
              ),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/appimages/bg1.jpg"),
                    fit: BoxFit.cover),
              ),
            ),
            new ListTile(
              onTap: () {},
              title: Text("Wishlist"),
              trailing: IconButton(
                icon: new Icon(Icons.bookmark),
                onPressed: () {},
              ),
            ),
            new ListTile(
              onTap: () {},
              title: Text("Account"),
              trailing: IconButton(
                icon: new Icon(Icons.verified_user),
                onPressed: () {},
              ),
            ),
            new ListTile(
              onTap: () {},
              title: Text("Setting"),
              trailing: IconButton(
                icon: new Icon(Icons.settings),
                onPressed: () {},
              ),
            ),
            new ListTile(
              onTap: () {
                Navigator.pushNamed(context, '/About');
              },
              title: new Text("About"),
              trailing: new IconButton(
                icon: new Icon(
                  Icons.info,
                  color: Colors.deepOrange[900],
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/About');
                },
              ),
            ),
            new ListTile(
              onTap: () {},
              title: Text("Logout"),
              trailing: IconButton(
                icon: new Icon(Icons.logout),
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
      body: new ListView(children: <Widget>[
        //edit

        Image.asset("assets/appimages/bg2.jpg"),

        //Listlogo
        Container(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
              color: Theme.of(context).dividerColor,
            )),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: new Icon(Icons.phone),
                    iconSize: 40,
                    color: Colors.deepOrange[900],
                    onPressed: () {
                      Navigator.pushNamed(context, '/kontak');
                    },
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8),
                    child: Text(
                      "Kontak",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Colors.deepOrange[900],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: new Icon(Icons.shop),
                    iconSize: 40,
                    color: Colors.deepOrange[900],
                    onPressed: () {
                      Navigator.pushNamed(context, '/home1');
                    },
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8),
                    child: Text(
                      "Daftar Pesanan",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Colors.deepOrange[900],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: new Icon(Icons.delivery_dining),
                    iconSize: 40,
                    color: Colors.deepOrange[900],
                    onPressed: () {
                      Navigator.pushNamed(context, '/pengiriman');
                    },
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8),
                    child: Text(
                      "Pengiriman",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Colors.deepOrange[900],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: new Icon(Icons.cake),
                    iconSize: 40,
                    color: Colors.deepOrange[900],
                    onPressed: () {
                      Navigator.pushNamed(context, '/produklist');
                    },
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8),
                    child: Text(
                      "Daftar Kue",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Colors.deepOrange[900],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),

        Container(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/appimages/bg3.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Produk Terlaris",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                      ),
                    ),
                    Text(
                      "Rekomendasi untuk anda",
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        new GestureDetector(
          onTap: () {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (BuildContext context) => DetailProduk(
                name: "Kue Coklat Karamel",
                description: "Kue dengan rasa coklat karamel",
                price: 150000,
                image: "2.jpg",
                star: 5,
              ),
            ));
          },
          child: ProductBox(
              name: "Kue Coklat Karamel",
              description: "Kue dengan rasa coklat karamel",
              price: 150000,
              image: "2.jpg",
              star: 5),
        ),

        new GestureDetector(
          onTap: () {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (BuildContext context) => DetailProduk(
                name: "Kue Cokelat Spesial",
                description: "Kue dengan rasa cokelat spesial",
                price: 200000,
                image: "4.jpg",
                star: 5,
              ),
            ));
          },
          child: ProductBox(
              name: "Kue Cokelat Spesial",
              description: "Kue dengan rasa cokelat spesial",
              price: 200000,
              image: "4.jpg",
              star: 5),
        ),
      ]),
    );
  }
}

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("About"),
          backgroundColor: Colors.red[400],
        ),
        body: new ListView(children: <Widget>[
          Image.asset("assets/appimages/bg2.jpg"),
          new Text("Aplikasi ini merupakan aplikasi untuk menjual kue"),
          new Text(
              "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."),
        ]));
  }
}

class Kontak extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Kontak"),
        backgroundColor: Colors.red[400],
      ),
      body: new Center(
        child: new Icon(
          Icons.phone,
          size: 50,
        ),
      ),
    );
  }
}

class Custom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Custom Kue"),
        backgroundColor: Colors.red[400],
      ),
      body: new Center(
        child: new Icon(
          Icons.shopping_cart,
          size: 50,
        ),
      ),
    );
  }
}

class Pengiriman extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Pengiriman"),
        backgroundColor: Colors.red[400],
      ),
      body: new Center(
        child: new Icon(
          Icons.delivery_dining,
          size: 50,
        ),
      ),
    );
  }
}
